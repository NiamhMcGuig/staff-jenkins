package com.citi.training.staff.dao;

import java.util.List;

import com.citi.training.staff.model.Employee;

public interface EmployeeRepo {

    List<Employee> findAll();

    Employee save(Employee employee);

    void deleteById(String id);
}
