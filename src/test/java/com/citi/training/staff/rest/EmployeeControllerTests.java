package com.citi.training.staff.rest;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

import com.citi.training.staff.model.Employee;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class EmployeeControllerTests {

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void test_save_findAll() {
        Employee testEmployee = new Employee();
        testEmployee.setName("Mr. REST INTEGRATION TEST");
        testEmployee.setAddress("123 Main Street");

        ResponseEntity<Employee> response = restTemplate.postForEntity("/v1/employee",
                                                                       testEmployee,
                                                                       Employee.class);

        assertEquals(response.getStatusCode(), HttpStatus.CREATED);

        ResponseEntity<List<Employee>> findAllResponse = restTemplate.exchange(
                                                        "/v1/employee",
                                                        HttpMethod.GET,
                                                        null,
                                                        new ParameterizedTypeReference<List<Employee>>(){});

        assertEquals(findAllResponse.getStatusCode(), HttpStatus.OK);
    }
}
